/** @type {import('next').NextConfig} */

const { parsed: ENV } = require("dotenv").config({
  allowEmptyValues: false,
  path: __dirname + `/src/env/${process.env.ENV || "dev"}.env`,
});

const path = require("path");

const nextConfig = {
  reactStrictMode: false,
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  async redirects() {
    return [
      {
        source: "/",
        destination: "/dashboard",
        permanent: false,
      },
    ];
  },
  env: {
    ENV_NAME: process.env.ENV,
    NEXT_PUBLIC_API: process.env.NEXT_PUBLIC_API,
  },
};

module.exports = nextConfig;
