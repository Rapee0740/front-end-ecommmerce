export interface IFromResponses<T> {
  result: boolean;
  statusCode: number;
  message: string;
  data?: T;
  [key: string]: any;
}

export type ThemeData = {
  borderRadius: number;
  colorPrimary: string;
  theme: boolean;
  compact: boolean;
};
