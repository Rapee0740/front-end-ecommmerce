import dynamic from "next/dynamic";
import React, { useEffect, useState } from "react";
type Props = {};

const Column = dynamic(
  async () => {
    const { Column } = await import("@ant-design/plots");
    return Column;
  },
  { ssr: false }
);

export default function columnPlotWithCornerRadius({}: Props) {
  const [data, setData] = useState([]);

  useEffect(() => {
    asyncFetch();
  }, []);

  const asyncFetch = () => {
    fetch(
      "https://gw.alipayobjects.com/os/antfincdn/PC3daFYjNw/column-data.json"
    )
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => {
        console.log("fetch data failed", error);
      });
  };

  const config = {
    data,
    xField: "city",
    yField: "value",
    seriesField: "type",
    isGroup: true,
    columnStyle: {
      radius: [20, 20, 0, 0],
    },
    color: ["#19CDD7", "#DDB27C", "#fcfcdc", "#000"],
  };

  return <Column {...config} />;
}
