import { ARR_ACCEPT_PICTURE } from "@/src/constants/accept-file";
import { PlusOutlined } from "@ant-design/icons";
import {
  Drawer,
  Space,
  Button,
  Form,
  Row,
  Col,
  Input,
  Select,
  Upload,
  message,
} from "antd";
import React, { useState } from "react";
import type { UploadProps } from "antd";
import type { UploadFile } from "antd/es/upload/interface";
import type { UploadChangeParam } from "antd/es/upload";

const { Option } = Select;

type Props = { setOpen: any; open: boolean };

export default function AddProduct({ setOpen, open }: Props) {
  const [loading, setLoading] = useState<boolean>(false);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [fileListBase64, setFileListBase64] = useState<any[]>([]);

  const onClose = () => {
    setOpen(false);
  };

  const props: UploadProps = {
    onRemove: (file: any) => {
      const index = fileList.indexOf(file);
      const newFileList: any = fileList.slice();
      newFileList.splice(index, 1);

      return setFileList(newFileList);
    },
    beforeUpload: (file: any) => {
      setFileList([...fileList, file]);
      return false;
    },
    onChange(info: any) {
      setLoading(true);
      const listFiles = info.fileList;
      setFileList(listFiles);

      const newArrayFiles = listFiles.map((file: any) =>
        file.originFileObj ? file.originFileObj : file
      );

      const anAsyncFunction = async (item: any) => {
        return convertBase64(item);
      };

      const getData = async () => {
        return Promise.all(
          newArrayFiles.map((item: any) => anAsyncFunction(item))
        );
      };

      getData().then((data) => {
        /*  setFileSend(data) */
        setFileListBase64(data);
        setLoading(false);
         console.log(data);
      });
    },
    // directory: true,
    fileList: fileList,
  };

  const convertBase64 = (file: File) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader?.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  // const handleChangeUpload: UploadProps["onChange"] = (
  //   info: UploadChangeParam<UploadFile>
  // ) => {
  //   const arrType = ARR_ACCEPT_PICTURE;
  //   // console.log("file", info.file);
  //   // console.log("fileList", fileList);
  //   if (info.file.status === "removed") {
  //     setFileList(info.fileList);
  //   } else {
  //     if (!arrType.includes(info.file.type)) {
  //       message.error(`${info.file.name} is not a Picture file`);
  //       setFileList(info.fileList.filter((m: any) => arrType.includes(m.type)));
  //       return;
  //     }

  //     let newFileList = [...info.fileList];
  //     // newFileList = newFileList.slice(-2);
  //     newFileList = newFileList.map((file) => {
  //       if (file.response) {
  //         file.url = file.response.url;
  //       }
  //       return file;
  //     });

  //     setFileList(newFileList.filter((m: any) => arrType.includes(m.type)));
  //   }
  // };

  return (
    <>
      <Drawer
        title="Create New Product"
        onClose={onClose}
        open={open}
        footer={
          <Row>
            <Col span={24} className="center-footer-btn">
              <Space>
                <Button onClick={onClose} type="primary">
                  Submit
                </Button>
                <Button onClick={onClose}>Cancel</Button>
              </Space>
            </Col>
          </Row>
        }
      >
        <Form layout="vertical">
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="nameProduct"
                label="ชื่อสินค้า"
                rules={[{ required: true, message: "กรุณากรอกชื่อสินค้า" }]}
              >
                <Input placeholder="กรุณากรอกชื่อสินค้า" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="description" label="Description">
                <Input.TextArea rows={4} placeholder="กรุณากรอกรายละเอียด" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Upload
                // beforeUpload={handleBeforeUpload}
                // onChange={handleChangeUpload}
                {...props}
                // fileList={fileList}
                listType="picture-card"
                multiple={true}
                showUploadList={{ showPreviewIcon: false }}
                accept={ARR_ACCEPT_PICTURE.toString()}
              >
                <div>
                  <PlusOutlined />
                  <div style={{ marginTop: 8 }}>Upload</div>
                </div>
              </Upload>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </>
  );
}
