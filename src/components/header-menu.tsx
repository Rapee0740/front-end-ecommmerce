import { Col, Typography, theme } from "antd";
import React from "react";

const { useToken } = theme;
const { Title, Text } = Typography;

type Props = {
  textHeader: string;
};

export default function headerMenu({ textHeader }: Props) {
  const { token } = useToken();

  return (
    <Col
      span={24}
      style={{
        position: "sticky",
        top: 0,
        zIndex: 1,
        backgroundColor: token.colorBgLayout,
        padding: 15,
        borderRadius: "0px 0px 10px 10px",
      }}
    >
      <Title
        level={3}
        style={{ fontWeight: "bold", margin: "0px", color: token.colorPrimary }}
      >
        {textHeader}
      </Title>
    </Col>
  );
}
