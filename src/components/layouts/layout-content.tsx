import React, { ReactNode } from "react";
import { Layout, theme } from "antd";

const { useToken } = theme;
const { Content } = Layout;

type Props = {
  children: ReactNode;
};

export default function LayoutContent({ children }: Props) {
  const { token } = useToken();

  return (
    <Content
      style={{
        minHeight: 280,
        overflow: "auto",
        zIndex: 0,
        backgroundColor: token.colorPrimaryBg,
      }}
    >
      {children}
    </Content>
  );
}
