import React from "react";
import {
  Avatar,
  Col,
  Dropdown,
  Layout,
  Row,
  Space,
  Typography,
  theme,
} from "antd";
import { UserOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { useRouter } from "next/router";
import { KEY_ROUTE } from "@/src/constants/routes";
import Link from "next/link";

const { Text } = Typography;
const { Header } = Layout;

type Props = {};

export default function LayoutHeder({}: Props) {
  const route = useRouter();
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const items: MenuProps["items"] = [
    {
      key: "logout",
      label: <Link href={KEY_ROUTE.LOGIN}>Logout</Link>,
    },
  ];
  return (
    <Header style={{ background: colorBgContainer }}>
      <Row>
        <Col
          span={24}
          style={{
            display: "flex",
            justifyContent: "end",
            alignItems: "center",
          }}
        >
          <Space direction="vertical" className="Space-Text-Avatar">
            <Text style={{ fontSize: "14px", fontWeight: "bold" }}>
              Rapee Thupchan(Boat)
            </Text>
            <Text style={{ fontSize: "11px" }}>Front End Devoloper</Text>
          </Space>

          <Dropdown menu={{ items }}>
            <Avatar style={{ cursor: "pointer" }} size={40}>
              B
            </Avatar>
          </Dropdown>
        </Col>
      </Row>
    </Header>
  );
}
