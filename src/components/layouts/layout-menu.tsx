import React, { Key, ReactNode, useState } from "react";
import { Col, Image, Layout, Menu, Row } from "antd";
import type { MenuProps } from "antd";
import { DesktopOutlined, PieChartOutlined } from "@ant-design/icons";
import { KEY_ROUTE } from "@/src/constants/routes";
import { useRouter } from "next/router";

const { Sider } = Layout;

type Props = {};

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: ReactNode,
  key: Key,
  icon?: ReactNode,
  children?: MenuItem[],
  type?: "group"
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

export default function LayoutMenu({}: Props) {
  const route = useRouter();
  const [collapsed, setCollapsed] = useState<boolean>(true);
  const [openKeys, setOpenKeys] = useState([KEY_ROUTE.DASHBOARD]);

  const items: MenuItem[] = [
    getItem("Dashboard", KEY_ROUTE.DASHBOARD, <PieChartOutlined />),
    getItem("Product", KEY_ROUTE.PRODUCT, <DesktopOutlined />),
  ];

  const rootSubmenuKeys = [KEY_ROUTE.DASHBOARD, KEY_ROUTE.PRODUCT];

  const onOpenChange: MenuProps["onOpenChange"] = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const onClickMenu = (values: any) => {
    route.push(values.key);
  };

  return (
    <Sider
      theme="light"
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
    >
      <Row>
        <Col>
          <Image
            src="/img/logo.jpg"
            preview={false}
            alt="logo"
            style={{ borderRadius: "30px" }}
          />
        </Col>
      </Row>
      <Menu
        mode="inline"
        selectedKeys={[route.asPath]}
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        onClick={onClickMenu}
        items={items}
      />
    </Sider>
  );
}
