import { Layout } from "antd";
import React, { ReactNode } from "react";
import LayoutMenu from "./layout-menu";
import LayoutHeder from "./layout-heder";
import LayoutContent from "./layout-content";

type Props = {
  children: ReactNode;
  validateHeader: boolean;
  validateSider: boolean;
};

export default function LayoutMain({
  children,
  validateHeader,
  validateSider,
}: Props) {
  return (
    <Layout style={{ height: "100vh" }}>
      {validateSider && <LayoutMenu></LayoutMenu>}
      <Layout className="site-layout">
        {validateHeader && <LayoutHeder></LayoutHeder>}
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
}
