import { Action, InitialState } from ".";

export const reducer = (
  state: InitialState,
  { type, payload }: Action
): InitialState => {
  switch (type) {
    case "SET_LOADING":
      return {
        ...state,
        loading: payload.loading,
      };
    default:
      return state;
  }
};
