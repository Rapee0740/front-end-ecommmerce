import { Dispatch, createContext } from "react";

export type ActionType = "SET_LOADING";

export interface InitialState {
  loading?: boolean;
}

export interface IContext {
  state: InitialState | null;
  dispatch: Dispatch<Action>;
}

export interface Action {
  type: ActionType;
  payload?: ActionPayload;
}

interface ActionPayload {
  loading?: boolean;
  [key: string]: any;
}

export const initialState: InitialState = {
  loading: false,
};

export const Context = createContext<IContext>(null);
