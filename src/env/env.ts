export const ENV = {
  ENV_NAME: process.env.ENV_NAME,
  NEXT_PUBLIC_API: process.env.NEXT_PUBLIC_API,
};
