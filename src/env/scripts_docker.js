var fs = require("fs");
console.log(" ------------------ start sync file ------------------ ");

let env = process.env.ENV;
console.log("[scripts docker] ENV : ", env);

const jsonPackage = require("../../package.json");

if (env === "prod") {
  var version_build = jsonPackage.version_build_prod;
} else if (env === "nonprod") {
  var version_build = jsonPackage.version_build_nonprod;
} else {
  var version_build = jsonPackage.version_build_dev;
}
console.log("version_build : ", version_build);

var docker_file = fs.readFileSync("src/env/docker/Dockerfile." + env, "utf-8");
fs.writeFileSync("Dockerfile", docker_file, "utf-8");
console.log(">>> sync Dockerfile complete.");

console.log(" ------------------ end sync file ------------------ ");
