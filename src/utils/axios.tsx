import axios from "axios";
// import { KEY_STORAGE } from "../constants/storage";
import { _getStorage } from "./local-storage";
import { IFromResponses } from "../types";

const setHeader = () => {
  // const getToken = _getStorage(KEY_STORAGE.SET_TOKEN)
  const headers = {
    "Content-Type": "application/json",
    // "Authorization" : `Bearer ${getToken}`
  };
  return headers;
};

const setHeaderUploadFile = () => {
  const headers = {
    "Content-Type": "multipart/form-data",
  };
  return headers;
};

const _postToken = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
  };
  return new Promise((resolve) => {
    axios.post(url, body, { headers, proxy: false }).then(
      async (result) => {
        console.log("result======: ", result);
        const data = {
          ...result.data,
          status: result.status,
          message: result.data.error_description,
        };
        resolve(data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _post = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeader();
  return new Promise((resolve) => {
    axios.post(url, body, { headers: headers, proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _postFile = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeaderUploadFile();
  return new Promise((resolve) => {
    axios.post(url, body, { headers: headers, proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _patch = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeader();
  return new Promise((resolve) => {
    axios.patch(url, body, { headers: headers, proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _get = async (
  url: string,
  params?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeader();
  return new Promise((resolve) => {
    axios.get(url, { headers, proxy: false, params }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};
const _delete = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeader();
  return new Promise((resolve) => {
    axios.delete(url, { headers, proxy: false, data: body }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const _put = async (
  url: string,
  body?: any,
  config?: any
): Promise<IFromResponses<any>> => {
  const headers = setHeader();
  return new Promise((resolve) => {
    axios.put(url, body, { headers, proxy: false }).then(
      (result) => {
        resolve(result.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const reTurnError = (error: any) => {
  if (error?.respones?.data?.status === 401) {
    const errJwt = {
      result: false,
      status: 401,
      message: "กรุณา Login เข้าสู่ระบบอีกครั้ง",
      data: "",
    };
    return errJwt;
  } else if (error?.response?.data) {
    console.log("reTurnError ===== else");
    return error.response.data;
  } else {
    return error;
  }
};

export { _postToken, _post, _get, _put, _delete, _patch, _postFile };
