import { ThemeData } from "../types";

export const defaultThemeData: ThemeData = {
  borderRadius: 6,
  colorPrimary: "#1677ff",
  theme: false,
  compact: false,
};
