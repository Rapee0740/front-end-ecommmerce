import ColumnPlotWithCornerRadius from "@/src/components/charts/column-plot-with-corner-radius";
import HeaderMenu from "@/src/components/header-menu";
import { Card, Col, Row, Typography, theme } from "antd";
import { useRouter } from "next/router";
import React from "react";

const { useToken } = theme;
const { Title } = Typography;

type Props = {};

export default function Dashboard({}: Props) {
  const { token } = useToken();
  const route = useRouter();

  return (
    <Row>
      <HeaderMenu textHeader={"Dashboard"} />

      <Col span={24} style={{ padding: "24px" }}>
        <Card>
          <Row>
            <Col span={24} style={{ padding: "20px" }}>
              <Title level={5}>รายงานขาย</Title>
              <ColumnPlotWithCornerRadius />
            </Col>

            <Col span={24} style={{ padding: "20px" }}>
              <Title level={5}>รายงานผู้ใช้งาน</Title>
              <ColumnPlotWithCornerRadius />
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  );
}
