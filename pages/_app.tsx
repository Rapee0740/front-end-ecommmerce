import LayoutMain from "@/src/components/layouts/layout-main";
import "@/styles/globals.css";
import "@/styles/index.scss";
import {
  ConfigProvider,
  Divider,
  FloatButton,
  Form,
  Modal,
  theme,
  Radio,
  InputNumber,
  Switch,
} from "antd";
import type { AppProps } from "next/app";
import { IoIosColorWand } from "react-icons/io";
import { ImSun } from "react-icons/im";
import { BsMoonFill } from "react-icons/bs";
import { useEffect, useReducer, useState } from "react";
import Head from "next/head";
import { reducer } from "@/src/context/reducers";
import { Context, initialState } from "@/src/context";
import { SketchPicker } from "react-color";
import { ThemeData } from "@/src/types";
import { defaultThemeData } from "@/src/constants";
import { _getStorage, _setStorage } from "@/src/utils/local-storage";
import { validateHeader, validateSider } from "@/src/shared/validate-layout";
import { useRouter } from "next/router";

const { darkAlgorithm, compactAlgorithm } = theme;

export default function App({ Component, pageProps }: AppProps) {
  const route = useRouter();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [dataTheme, setDataTheme] = useState<ThemeData>(defaultThemeData);
  const store = { state, dispatch };
  const [form] = Form.useForm();

  useEffect(() => {
    if (_getStorage("Theme")) {
      setDataTheme(_getStorage("Theme"));
    } else {
      setDataTheme(defaultThemeData);
    }
  }, []);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleThemes = (changedValues, allValues) => {
    const colorObj = {
      colorPrimary: allValues?.colorPrimary?.hex
        ? allValues?.colorPrimary?.hex
        : allValues.colorPrimary,
    };
    setDataTheme({
      ...allValues,
      ...colorObj,
    });
    _setStorage("Theme", {
      ...allValues,
      ...colorObj,
    });
  };

  const algorithmCheck = () => {
    if (dataTheme.theme && dataTheme.compact) {
      return [darkAlgorithm, compactAlgorithm];
    } else if (!dataTheme.theme && dataTheme.compact) {
      return [compactAlgorithm];
    } else if (dataTheme.theme && !dataTheme.compact) {
      return [darkAlgorithm];
    } else {
      return [];
    }
  };

  return (
    <Context.Provider value={store}>
      <ConfigProvider
        theme={{
          algorithm: algorithmCheck(),
          token: {
            colorPrimary: dataTheme.colorPrimary,
            borderRadius: dataTheme.borderRadius,
          },
        }}
      >
        <Head>
          <title>ไม้อัด Lanna</title>
          <link rel="icon" href="/img/logo.jpg" />
        </Head>
        <LayoutMain
          validateHeader={validateHeader(route.pathname)}
          validateSider={validateSider(route.pathname)}
        >
          <Component {...pageProps} />
          <FloatButton icon={<IoIosColorWand />} onClick={() => showModal()} />
          <Modal
            title="Customize Theme"
            open={isModalOpen}
            onCancel={handleCancel}
            footer={null}
          >
            <Divider />
            <Form
              form={form}
              name="theme"
              initialValues={dataTheme}
              onValuesChange={(changedValues, allValues) =>
                handleThemes(changedValues, allValues)
              }
            >
              <Form.Item label="Theme" name="theme" valuePropName="checked">
                <Switch
                  checkedChildren={<BsMoonFill />}
                  unCheckedChildren={<ImSun />}
                />
              </Form.Item>
              <Form.Item
                valuePropName="color"
                name="colorPrimary"
                label="Primary Color"
              >
                <SketchPicker />
              </Form.Item>
              <Form.Item name="borderRadius" label="Border Radius">
                <InputNumber />
              </Form.Item>
              <Form.Item name="compact" label="Compact">
                <Radio.Group>
                  <Radio value={false}> Default </Radio>
                  <Radio value={true}> Compact </Radio>
                </Radio.Group>
              </Form.Item>
            </Form>
          </Modal>
        </LayoutMain>
      </ConfigProvider>
    </Context.Provider>
  );
}
