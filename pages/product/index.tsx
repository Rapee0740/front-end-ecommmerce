import { KEY_ROUTE } from "@/src/constants/routes";
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Row,
  Space,
  Table,
  Tag,
  Typography,
} from "antd";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import type { ColumnsType } from "antd/es/table";
import { PlusCircleOutlined } from "@ant-design/icons";
import AddProduct from "@/src/components/product/add-product";
import { useRouter } from "next/router";
import { ENV } from "@/src/env/env";
import HeaderMenu from "@/src/components/header-menu";

const { Text } = Typography;

type Props = {};

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
}

export default function Product({}: Props) {
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [currentUrl, setCurrentUrl] = useState(router.pathname);

  useEffect(() => {
    const handleLocationChange = () => {
      setCurrentUrl(router.pathname);
    };

    window.addEventListener("popstate", handleLocationChange);
    return () => {
      window.removeEventListener("popstate", handleLocationChange);
    };
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Tags",
      key: "tags",
      dataIndex: "tags",
      render: (_, { tags }) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 5 ? "geekblue" : "green";
            if (tag === "loser") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <a>Invite {record.name}</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      tags: ["nice", "developer"],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      tags: ["loser"],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sidney No. 1 Lake Park",
      tags: ["cool", "teacher"],
    },
  ];

  const showAddProduct = () => {
    setOpen(true);
  };

  return (
    <Row>
      <HeaderMenu textHeader={"Product"} />
      <Col span={24} style={{ padding: "24px" }}>
        <Card>
          <Row>
            <Col span={24} style={{ display: "flex", justifyContent: "end" }}>
              <Button
                type="primary"
                size="large"
                icon={<PlusCircleOutlined />}
                style={{ margin: "10px" }}
                onClick={showAddProduct}
              >
                Add Product
              </Button>
            </Col>
            <Col span={24}>
              <Table columns={columns} dataSource={data} />
            </Col>
          </Row>
        </Card>

        <AddProduct setOpen={setOpen} open={open}></AddProduct>
      </Col>
    </Row>
  );
}
