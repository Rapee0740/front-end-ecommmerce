import { KEY_ROUTE } from "@/src/constants/routes";
import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  Row,
  Typography,
} from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

const { Title, Text } = Typography;

type Props = {};

export default function Index({}: Props) {
  const route = useRouter();

  const onFinishFailed = () => {
    route.push(KEY_ROUTE.DASHBOARD);
  };
  return (
    <Row className="login-form">
      <Col className="col-login">
        <Card className="card-login">
          <Title level={4} className="title-login">
            Login
          </Title>
          <Form
            name="Login"
            initialValues={{ remember: true }}
            // onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="Email" />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password placeholder="password" />
            </Form.Item>

            <Form.Item name="remember" valuePropName="checked">
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" block>
                Login
              </Button>
            </Form.Item>
          </Form>

          <Row>
            <Col className="col-forget-password">
              <Link className="text-forget-password" href={KEY_ROUTE.DASHBOARD}>
                Forget password ?
              </Link>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  );
}
